function lineDistance(x, y, x0, y0){
    return Math.sqrt((x -= x0) * x + (y -= y0) * y);
};

function drawLine(a, b, line) {
  let pointA = $(a).offset();
  let pointB = $(b).offset();
  let pointAcenterX = $(a).width() / 2;
  let pointAcenterY = $(a).height() / 2;
  let pointBcenterX = $(b).width() / 2;
  let pointBcenterY = $(b).height() / 2;
  let angle = Math.atan2(pointB.top - pointA.top, pointB.left - pointA.left) * 180 / Math.PI;
  let distance = lineDistance(pointA.left, pointA.top, pointB.left, pointB.top);

  // Set Angle
  $(line).css('transform', 'rotate(' + angle + 'deg)');

  // Set Width
  $(line).css('width', distance + 'px');
                  
  // Set Position
  $(line).css('position', 'absolute');
  if(pointB.left < pointA.left) {
    $(line).offset({top: pointA.top + pointAcenterY, left: pointB.left + pointBcenterX});
  } else {
    $(line).offset({top: pointA.top-215 + pointAcenterY, left: pointA.left + pointAcenterX});
  }
}

setInterval(function() {
  drawLine('.camera', '.customSlideContentContainer.white h1', '.line');
});

