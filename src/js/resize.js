jQuery(function($) {
    $(document).ready(function () {
        function resizeTablet(currentInnerWidth) {
            if (currentInnerWidth <= 1460 && currentInnerWidth >= 1290) {
                $("#tablet").css('width', '668px');
            }
            if (currentInnerWidth <= 1289 ) {
                $("#tablet").css('width', '568px');
            }

            if (currentInnerWidth >= 1290 && currentInnerWidth <= 1460) {
                $("#tablet").css('width', '668px');
            }
            if (currentInnerWidth >= 1460) {
                $("#tablet").css('width', '768px');
            }
        }        
        $(window).resize( function(e) {
            console.log("resized", e.target.innerWidth);
            let currentInnerWidth = e.target.innerWidth;
            resizeTablet(currentInnerWidth);
        });      
    });
});
  