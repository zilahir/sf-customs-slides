var gulp = require('gulp'),
    connect = require('gulp-connect'),    
    sass = require('gulp-sass'),
    minifyCSS = require('gulp-minify-css');
    rename = require('gulp-rename'),
    ignore = require('gulp-ignore'),
    changed = require('gulp-changed'),
    uglifyjs = require('uglify-es'),
    composer = require('gulp-uglify/composer'),
    minify = composer(uglifyjs, console);
    source = './src/',
    dest = './dist/';


var gothamFont = {
    in: source + "./fonts/gotham/"
};

var fonts = {
  in: [ gothamFont.in + '*' ],
    out: dest + 'fonts/'
}

var scss = {
    in: source + 'scss/main.scss',
    out: dest + 'css/',
    watch: source + 'scss/**/*',
    sassOpts: {
        outputStyle: 'nested',
        precison: 3,
        errLogToConsole: true,
    }
};

gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

gulp.task('copySassLibs', function() {
    var flexgrid = gulp.src(['node_modules/flexgrid-sass/assets/**/*']).pipe(gulp.dest('./scss/lib/flexgrid-sass/assets'));
});


gulp.task('sass', ['minify-sass'], function () {
    return gulp.src(scss.in)
        .pipe(sass(scss.sassOpts))
        .pipe(gulp.dest(scss.out))
});

gulp.task('minify-sass', function () {
    return gulp.src(scss.in)
        .pipe(sass(scss.sassOpts))
        .pipe(minifyCSS())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest(scss.out))
});

gulp.task('copyJsLibs', function() {
    gulp.src([
        './node_modules/jquery/dist/jquery.min.js'
    ]).pipe(gulp.dest(dest + 'js/lib'));
});


// default task
gulp.task('default', ['sass', 'minify-sass', 'copyJsLibs'], function () {
    gulp.watch(scss.watch, ['sass']);
});
